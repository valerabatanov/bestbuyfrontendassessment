// Karma configuration
// Generated on Thu Nov 19 2015 01:03:22 GMT-0800 (PST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // coverage reporter generates the coverage
    reporters: ['progress', 'coverage'],

    // list of files / patterns to load in the browser
    files: [
    'public_html/js/angular/angular.js',
    'public_html/js/angular/angular-ui-router.js',
    'public_html/js/angular/angular-sanitize.js',
    'public_html/js/angular/angular-mocks.js',
    'public_html/js/angular/mock-helpers.js',
    'public_html/js/*.js',
    'public_html/tests/js/*.js',
    'public_html/html/**/*.html'
    ],


    // list of files to exclude
    exclude: [
    ],

    // coverage reporter generates the coverage
    reporters: ['progress', 'coverage'],

    preprocessors: {

      'public_html/js/*.js': ['coverage'],
      'public_html/html/**/*.html': ['ng-html2js'] 
    },
  ngHtml2JsPreprocessor: {
      'moduleName': 'Templates',
      stripPrefix: "public_html/",
     prependPrefix: '/',
       
    },
    // optionally, configure the reporter
    coverageReporter: {
      type : 'html',
      dir : 'coverage/'
    },
plugins: [  
             'karma-phantomjs-launcher',
             'karma-jasmine',
             'karma-ng-html2js-preprocessor',
             'karma-coverage',
             'karma-chrome-launcher'
],
    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultanous
    concurrency: Infinity
  })
}
