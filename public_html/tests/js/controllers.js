describe('controlles', function() {
    beforeEach(module('controllers'));
    describe('categoriesController', function() {
        it('should read categories from data service', function() {
            var currentCategoryID = 2001;
            var serviceResponce = [
                {
                    "id": "20001",
                    "name": "Computers & Tablets",
                    "hasSubcategories": true,
                    "productCount": 4903
                }];
            module(function($provide) {
                $provide.value('dataService', {getAllcategories: function() {
                        return getSuccessfulPromise(serviceResponce)
                    }});
                $provide.value('$state', {params: {categoryId: currentCategoryID}});
            });
            inject(function($controller, $rootScope) {
                var newScope = $rootScope.$new();
                var controller = $controller('categoriesController', {$scope: newScope});
                $rootScope.$digest();
                expect(newScope.categories).toEqual(serviceResponce);

            });
        });
    });

    describe('productsController', function() {
        var serviceResponceFirstPage = getCategoryInfo(1, 100, getTestProduct());
        var serviceResponceLastPage = getCategoryInfo(100, 100, getTestProduct());
        var currentCategoryID = 2001;

        it('should read category products from data service', function() {


            module(function($provide) {
                $provide.value('dataService', {getProducts: function(categoriId, page) {
                        if (categoriId === currentCategoryID) {

                            return getSuccessfulPromise(serviceResponceFirstPage);
                        }
                    }

                });
                $provide.value('$state', {params: {categoryId: currentCategoryID}});

            });
            inject(function($controller, $rootScope) {
                var newScope = $rootScope;
                var controller = $controller('productsController', {$scope: newScope});
                $rootScope.$digest();
                expect(newScope.catProducts).toEqual(serviceResponceFirstPage);
            });
        });

        it('should move next page on category and read products from data service', function() {


            module(function($provide) {
                $provide.value('dataService', {getProducts: function(categoriId, page) {
                        if (categoriId === currentCategoryID) {
                            if (page > 1) {
                                return getSuccessfulPromise(serviceResponceLastPage);

                            } else {
                                return getSuccessfulPromise(serviceResponceFirstPage);
                            }
                        }
                    }

                });
                $provide.value('$state', {params: {categoryId: currentCategoryID}});

            });
            inject(function($controller, $rootScope) {
                var newScope = $rootScope;
                var controller = $controller('productsController', {$scope: newScope});
                $rootScope.$digest();
                expect(newScope.catProducts).toEqual(serviceResponceFirstPage);
                // next page should load LastPage response
                newScope.nextPage();
                expect(newScope.catProducts).toEqual(serviceResponceLastPage);
                // next page shouldn't load any data
                newScope.nextPage();

            });
        });

        it('should move prev page on category and read products from data service', function() {

            module(function($provide) {
                $provide.value('dataService', {getProducts: function(categoriId, page) {
                        if (categoriId === currentCategoryID) {
                            if (page === 1) {
                                return getSuccessfulPromise(serviceResponceLastPage);

                            } else {
                                return getSuccessfulPromise(serviceResponceFirstPage);
                            }
                        }
                    }

                });
                $provide.value('$state', {params: {categoryId: currentCategoryID}});

            });
            inject(function($controller, $rootScope) {
                var newScope = $rootScope;
                var controller = $controller('productsController', {$scope: newScope});
                $rootScope.$digest();
                expect(newScope.catProducts).toEqual(serviceResponceLastPage);
                // prev page should load LastPage response
                newScope.prevPage();
                expect(newScope.catProducts).toEqual(serviceResponceFirstPage);
                // prev page shouldn't load any data
                newScope.prevPage();

            });
        });
    });

    describe('productController', function() {
        it('should read product information from data service', function() {
            var product = getTestProduct();
            module(function($provide) {
                $provide.value('dataService', {getProduct: function() {
                        return getSuccessfulPromise(product);
                    }});
                $provide.value('$state', {params: {sku: product.sku}});

            });
            inject(function($controller, $rootScope) {
                var newScope = $rootScope.$new();
                $controller('productController', {$scope: newScope});
                $rootScope.$digest();
                expect(newScope.product).toEqual(product);
                expect(newScope.viewProduct).toEqual(true);
                newScope.closeView();
                expect(newScope.viewProduct).toEqual(false);

            });
        });
    });
});

