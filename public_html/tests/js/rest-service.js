describe('services', function() {
    beforeEach(module('rest'));
    describe('rest-service', function() {
        var baseUrls = {'siteUrl': 'http://www.bestbuy.ca', 'apiUrl': 'http://www.bestbuy.ca/api/v2/json'};
        it('should return site url', function() {
            module(function($provide) {
                $provide.value('$http', {});
                $provide.value('baseUrls', baseUrls);
            });
            inject(function(bbRest, $rootScope) {

                expect(bbRest.getSiteUrl()).toEqual(baseUrls.siteUrl);
                $rootScope.$digest();

            });
        });
        it('should return ctaegories data without error', function() {
            module(function($provide) {
                $provide.value('$http', {});
                $provide.value('baseUrls', baseUrls);

            });
            var mockResponse = getAllCategoriesTest();

            inject(function(bbRest, $rootScope) {
                bbRest.getjQuery = function(options) {
                    options.success(mockResponse);
                };
                bbRest.get('/category/Departments').then(function(data) {
                    expect(data).toEqual(mockResponse);
                });
                $rootScope.$digest();

            });
        });
        it('should return ctaegories shoudl return error', function() {
            module(function($provide) {
                $provide.value('$http', {});
                $provide.value('baseUrls', baseUrls);

            });
            inject(function(bbRest, $rootScope) {
                bbRest.getjQuery = function(options) {
                    options.error();
                };
                bbRest.get('/category/Departments').then(function() {
                    throw new Error('do called success callback but should not');
                }, function(err) {
                    expect(err).toEqual('Error responce');
                });

                $rootScope.$digest();

            });
        });
    });
});