describe('List View', function() {
    beforeEach(module('list'));
    beforeEach(module('Templates'));

    describe('list-view directive', function() {


        it('should show data after loaded', inject(function($compile, $rootScope, $timeout) {
            var scope = $rootScope.$new();
            scope.testSource = ['data!!'];
            scope.loadPromise = getSuccessfulPromise({});

            var element = $compile('<list-view  data-source="testSource" data-loading-promise="loadPromise"></list-view>')(scope);
            scope.$digest();
            $timeout.flush();
            var loading = element.find('.loader');
            expect(loading.length).toBe(0);
      
        }));
        it('should not show anything', inject(function($compile, $rootScope, $timeout) {
            var scope = $rootScope.$new();
            scope.testSource = {};

            var element = $compile('<list-view   ></list-view>')(scope);
            scope.$digest();
            $timeout.flush();
            var loading = element.find('.loader');
            expect(loading.length).toBe(0);
      
        }));
    });


});
