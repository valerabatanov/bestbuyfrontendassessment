describe('services', function() {
    beforeEach(module('data-service'));
    describe('data-service', function() {
        it('should get all categories without error', function() {
            var mockResponse = getAllCategoriesTest();
            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if (url === '/category/Departments') {
                            return getSuccessfulPromiseOnce(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {

                dataService.getAllcategories().then(function(data) {
                    expect(data).toEqual(mockResponse.subCategories);
                });
                // load from cach 
                dataService.getAllcategories().then(function(data) {
                    expect(data).toEqual(mockResponse.subCategories);
                });
                $rootScope.$digest();

            });
        });

        it('should get all categories with error', function() {
            var mockResponse = errorRespince('error!!!');
            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if (url === '/category/Departments') {
                            return getFailedPromise(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {
                dataService.getAllcategories().then(function() {
                    throw new Error('do called success callback but should not');
                }, function(err) {
                    expect(err).toEqual(mockResponse);
                });
                $rootScope.$digest();

            });
        });

        it('should get category products without error', function() {
            var mockResponse = getCategoryInfo(1, 100, getTestProduct());
            var categoryID = 2001;
            var page = 20;
            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if ((url === '/search?categoryId=' + categoryID + '&page=' + page) || (url === '/search?categoryId=' + categoryID)) {
                            return getSuccessfulPromise(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {

                dataService.getProducts(categoryID, page).then(function(data) {
                    expect(data).toEqual(mockResponse);
                });
                dataService.getProducts(categoryID).then(function(data) {
                    expect(data).toEqual(mockResponse);
                });
                $rootScope.$digest();

            });
        });

        it('should get category products with error', function() {
            var mockResponse = errorRespince('error!!!');
            var categoryID = 2001;
            var page = 20;
            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if ((url === '/search?categoryId=' + categoryID + '&page=' + page) || (url === '/search?categoryId=' + categoryID)) {
                            return getFailedPromise(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {

                dataService.getProducts(categoryID, page).then(function() {
                    throw new Error('do called success callback but should not');
                }, function(err) {
                    expect(err).toEqual(mockResponse);
                });

                $rootScope.$digest();

            });
        });

        it('should get product information without error', function() {
            var mockResponse = getTestProduct();

            var sku = 'test-sku';

            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if ((url === '/product/' + sku)) {
                            return getSuccessfulPromise(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {

                dataService.getProduct(sku).then(function(data) {
                    expect(data).toEqual(mockResponse);
                });

                $rootScope.$digest();

            });
        });
        it('should get product and update thumbnailImage information without error', function() {
            var mockResponse = getTestProductWiththumbnailImage();
            var mockResponseFormated = getTestProductWiththumbnailImage();

            var sku = 'test-sku';
            var siteUrl = 'http://www.bestbuy.ca';
            mockResponseFormated.thumbnailImage = siteUrl + mockResponseFormated.thumbnailImage;

            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if ((url === '/product/' + sku)) {
                            return getSuccessfulPromise(mockResponse);
                        }
                    },
                    getSiteUrl: function() {
                        return siteUrl;
                    }
                });
            });
            inject(function(dataService, $rootScope) {

                dataService.getProduct(sku).then(function(data) {
                    expect(data).toEqual(mockResponseFormated);
                });

                $rootScope.$digest();

            });
        });
        it('should get product information with error', function() {
            var mockResponse = errorRespince('error!!!');
            var sku = 'test-sku';
            module(function($provide) {
                $provide.value('bbRest', {get: function(url) {
                        if ((url === '/product/' + sku)) {
                            return getFailedPromise(mockResponse);
                        }
                    }});
            });
            inject(function(dataService, $rootScope) {

                dataService.getProduct(sku).then(function() {
                    throw new Error('do called success callback but should not');
                }, function(err) {
                    expect(err).toEqual(mockResponse);
                });

                $rootScope.$digest();

            });
        });
    });
});