describe('Testing Routes', function() {

    beforeEach(module('ui.router'));
    // load the controller's module
    beforeEach(module('webApp'));
    it('should be /category/departments by default', function() {
        module(function($provide) {
            $provide.value('ngRoute', {});
        });
        inject(function($state, $rootScope, $templateCache) {
            $templateCache.put('/html/categories/list.html', '');
            $templateCache.put('/html/products/list.html', '');
            //$state.go('');
            $rootScope.$digest();
            expect($state.current.url).toBe('/category/:categoryId');
        });
    });
    it('should be /category/:categoryId for category', function() {
        module(function($provide) {
            $provide.value('ngRoute', {});
        });
        inject(function($state, $rootScope, $templateCache) {
            $templateCache.put('/html/categories/list.html', '');
            $templateCache.put('/html/products/list.html', '');
            $state.go('category');
            $rootScope.$digest();
            expect($state.current.url).toBe('/category/:categoryId');
        });
    });
    it('should be /:sku for category.product', function() {
        module(function($provide) {
            $provide.value('ngRoute', {});
        });
        inject(function($state, $rootScope, $templateCache) {
            $templateCache.put('/html/categories/list.html', '');
            $templateCache.put('/html/products/list.html', '');
            $templateCache.put('/html/products/view.html', '');

            $state.go('category.product');
            $rootScope.$digest();
            expect($state.current.url).toBe('/:sku');
        });
    });
});