function mockTemplate(templateRoute, tmpl) {
  $templateCache.put(templateRoute, tmpl || templateRoute);
}
function goTo(url) {
  $location.url(url);
  $rootScope.$digest();
}

function goFrom(url) {
  return {toState: function (state, params) {
    $location.replace().url(url); //Don't actually trigger a reload
    $state.go(state, params);
    $rootScope.$digest();
  }};
}

function resolve(value) {
  return {forStateAndView: function (state, view) {
    var viewDefinition = view ? $state.get(state).views[view] : $state.get(state);
    return $injector.invoke(viewDefinition.resolve[value]);
  }};
}
function errorRespince(message) {
    return {
        "error": message
    };
}

function getTestProduct() {
    return {
        "sku": "10392124",
        "name": "HP Pavilion 15.6\" Laptop - Silver (AMD A10-8700P APU / 1TB HDD / 8GB RAM / Windows 10)",
        "regularPrice": 749.99,
        "salePrice": 749.99,
        "shortDescription": "Watch more, play more, and store more, all in style, with this HP Pavilion 15.6\" laptop. It features 1.8GHz AMD quad-core processor, with 8GB of RAM, and Radeon R6 graphics with up to 4352MB of dedicated memory. A big 1TB hard lets you take your work and multimedia collection with you, while WiFi, Bluetooth, USB 3.0 support let you connect and expand.",
        "productType": null
    }
}
function getTestProductWiththumbnailImage() {
    return {
        "sku": "10392124",
        "name": "HP Pavilion 15.6\" Laptop - Silver (AMD A10-8700P APU / 1TB HDD / 8GB RAM / Windows 10)",
        "regularPrice": 749.99,
        "salePrice": 749.99,
        "shortDescription": "Watch more, play more, and store more, all in style, with this HP Pavilion 15.6\" laptop. It features 1.8GHz AMD quad-core processor, with 8GB of RAM, and Radeon R6 graphics with up to 4352MB of dedicated memory. A big 1TB hard lets you take your work and multimedia collection with you, while WiFi, Bluetooth, USB 3.0 support let you connect and expand.",
        "productType": null,
        "thumbnailImage": "/multimedia/products/150x150/103/10394/10394340.jpg"
    }
}
function getAllCategoriesTest() {
    return {
        "Brand": "BestBuyCanada",
        "productCount": 70635,
        "id": "Departments",
        "parentCategories": [],
        "name": "Departments",
        "subCategories": [
            {
                "id": "20001",
                "name": "Computers & Tablets",
                "hasSubcategories": true,
                "productCount": 4941
            },
            {
                "id": "20003",
                "name": "TV & Home Theatre",
                "hasSubcategories": true,
                "productCount": 1397
            }]
    };
}
function getCategoryInfo(currentPage, totalPages, products) {
    return {
        "Brand": "BestBuyCanada",
        "currentPage": currentPage,
        "total": 4903,
        "totalPages": totalPages,
        "pageSize": 32,
        "products": products
    };
}
function getSuccessfulPromise(response) {
    var newResponse = angular.copy(response);
    return {
        then: function(success) {
            success(newResponse);
            return getSuccessfulPromiseOnce(newResponse);
        },
        finally: function(success) {
            return success(newResponse);
        }
    };
}

function getFailedPromise(response) {
    var newResponse = angular.copy(response);
    return {
        then: function(success, fail) {
            fail(newResponse);
        },
        catch: function(fail) {
            return fail(newResponse);
        },
        finally: function(fail) {
            return fail(newResponse);
        }
    };
}

function getSuccessfulPromiseOnce(response) {
    var newResponse = angular.copy(response);
    var count = 0;
    return {
        then: function(success) {
            if (count === 0) {
                success(newResponse);
            }
            count++;
        },
        finally: function(success) {
            if (count === 0) {
                success(newResponse);
            }
            count++;
        }
    };
}