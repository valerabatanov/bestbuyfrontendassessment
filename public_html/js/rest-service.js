var rest = angular.module('rest', []);

/**
 * @ngdoc service
 * @name rest.service:bbRest
 * @description
 * REST API service wrapper.
 * I am using Query ajax instead of $http service because server-side  of www.bestbuy.ca have restriction for 
 * Cross- domain requests so I had this error:
 * "XMLHttpRequest cannot load http://www.bestbuy.ca/api/v2/json/category/Departments. No 'Access-Control-Allow-Origin' header is present on the requested resource.
 * Origin 'http://localhost' is therefore not allowed access."
 * So if you  will run on the same domain it would be better to use $http
 * 
 */
rest.factory('bbRest', ['$http', '$q', 'baseUrls', function($http, $q, baseUrls) {

        function makeRequest(url) {
            var fullUrl = baseUrls.apiUrl + url;
            var defer = $q.defer();
            init.getjQuery({
                type: 'GET',
                url: fullUrl,
                dataType: 'jsonp',
                success: function(data) {
                    defer.resolve(data);
                },
                error: function() {
                    defer.reject('Error responce');
                }
            });


            return defer.promise;
        }
        var init =
                {
                    /**
                     * @ngdoc method
                     * @name get
                     * @methodOf rest.service:bbRest
                     * @description
                     * REST API GET request
                     *
                     * @param {string} url REST API GET request url
                     * @returns {promise} Promise that gets resolved when server responds to request
                     */
                    get: function(url) {
                        return makeRequest(url);
                    },
                    /**
                     * @ngdoc method
                     * @name getSiteUrl
                     * @methodOf rest.service:bbRest
                     * @description
                     * Return site Url form configuration
                     *
                     * @returns {string} site Url
                     */
                    getSiteUrl: function() {
                        return baseUrls.siteUrl;
                    },
                    /**
                     * @ngdoc method
                     * @name getjQuery
                     * @methodOf rest.service:bbRest
                     * @description
                     * Return Query ajax object 
                     *
                     * @returns {object} ajax object
                     */
                    getjQuery: function(options) {
                        return jQuery.ajax(options);
                    }
                };
        return init;
    }]);
