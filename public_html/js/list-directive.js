var list = angular.module('list', []);

/**
 * @ngdoc directive
 * @name list.directive:listView
 * @scope
 * @restrict E
 *
 * @description
 * List view that gives loading notification
 *
 * @param {object} source Object used to populate list view
 * @param {promise} loadingPromise Promise that loads source object
 * @param {string} itemTemplate template for item
 */
list.directive('listView', ['$timeout', function($timeout) {
        return {
            restrict: 'E',
            transclude: true,
            scope: {
                source: '=?',
                loadingPromise: '=?',
                itemTemplate: '=?'
            },
            controller: function($scope) {
                $scope.items = $scope.source;
                $scope.isLoading = false;
                $scope.itemTemplate = $scope.itemTemplate;
                if ($scope.loadingPromise) {
                    $scope.$watch('loadingPromise', function() {
                        $scope.isLoading = true;
                        $scope.loadingPromise.then(function() {
                            // timeout will make sure this gets executed after all those that are waiting for the loadingPromise
                            $timeout(function() {
                                $scope.items = $scope.source;

                            }, 0);
                        }).finally(function() {
                            $scope.isLoading = false;
                        });
                    });
                }


            },
            templateUrl: '/html/list/loading.html'
        };

    }]);