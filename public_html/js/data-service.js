var dataService = angular.module('data-service', []);


/**
 * @ngdoc service
 * @name dataService
 * @description
 * Manages REST API communication for catalog
 */
dataService.service('dataService', ['bbRest', '$q', function(bbRest, $q) {
        var cached = {
            categories: [],
            categoreisIsLoaded: false
        };


        var init = {
            /**
             * @ngdoc method
             * @name getAllcategories
             * @methodOf dataService
             * @description
             * Load list of categories
             *
             * @returns {promise} Promise that resolves when category list has been retrieved
             */
            getAllcategories: function() {
                var defer = $q.defer();
                if (!cached.categoreisIsLoaded)
                {
                    // get categories list data
                    bbRest.get('/category/Departments').then(function(data) {
                        cached.categoreisIsLoaded = true;
                        cached.categoreis = data.subCategories;
                        defer.resolve(data.subCategories);
                    }, function(err) {
                        defer.reject(err);
                    });
                } else {
                    defer.resolve(cached.categoreis);

                }
                return defer.promise;
            },
            /**
             * @ngdoc method
             * @name getProducts
             * @methodOf dataService
             * @description
             * Load list of products for category
             *
             * @returns {promise} Promise that resolves when products list has been retrieved
             */
            getProducts: function(categoryID, page) {
                var defer = $q.defer();
                // get products list data
                bbRest.get('/search?categoryId=' + categoryID + (angular.isNumber(page) ? '&page=' + page : '')).then(function(data) {
                    defer.resolve(data);
                }, function(err) {
                    defer.reject(err);
                });
                return defer.promise;

            },
            /**
             * @ngdoc method
             * @name getProduct
             * @methodOf dataService
             * @description
             * Load product data 
             *
             * @returns {promise} Promise that resolves when product data has been retrieved
             */
            getProduct: function(sku) {
                var defer = $q.defer();
                // get products data
                bbRest.get('/product/' + sku).then(function(data) {
                    if (data.thumbnailImage) {
                        data.thumbnailImage = bbRest.getSiteUrl() + data.thumbnailImage;
                    }
                    defer.resolve(data);
                }, function(err) {
                    defer.reject(err);
                });
                return defer.promise;

            }
        };

        return init;

    }]);
