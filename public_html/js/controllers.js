var controllers = angular.module('controllers', []);

/**
 * @ngdoc controller
 * @name categoriesController
 * @description
 * Controller for categories view
 */
controllers.controller('categoriesController', ['$scope', '$state', 'dataService', function($scope, $state, dataService) {
        $scope.currentCategoryId = $state.params.categoryId;
        $scope.loadingCategories = dataService.getAllcategories();
        $scope.loadingCategories.then(function(categories) {
            $scope.categories = categories;
        })

    }]);

/**
 * @ngdoc controller
 * @name productsController
 * @description
 * Controller for category products view
 */
controllers.controller('productsController', ['$scope', '$state', 'dataService', function($scope, $state, dataService) {
        //Protected method to load category ptoducts data 
        function loadCategory(categoryId, page) {
            $scope.loadingProducts = dataService.getProducts(categoryId, page);
            $scope.loadingProducts.then(function(catProducts) {
                $scope.catProducts = catProducts;
            });
        }

        loadCategory($state.params.categoryId, 1);
        // Callback for product list navigation next page 
        $scope.nextPage = function() {
            if ($scope.catProducts.currentPage < $scope.catProducts.totalPages) {
                loadCategory($state.params.categoryId, $scope.catProducts.currentPage + 1);
            }
        }
        // Callback for product list navigation prev page
        $scope.prevPage = function() {
            if ($scope.catProducts.currentPage > 1) {
                loadCategory($state.params.categoryId, $scope.catProducts.currentPage - 1);
            }
        }
    }]);

/**
 * @ngdoc controller
 * @name productController
 * @description
 * Controller for product view
 */
controllers.controller('productController', ['$scope', '$state', 'dataService', function($scope, $state, dataService) {
        $scope.productLoading = false;
        $scope.viewProduct = false;
        $scope.viewProduct = true;
        // Callback for close product modal window
        $scope.closeView = function() {
            $scope.viewProduct = false;
        }
        $scope.productLoading = true;
        dataService.getProduct($state.params.sku).then(function(product) {
            $scope.product = product;
            $scope.productLoading = false;
        });

    }]);