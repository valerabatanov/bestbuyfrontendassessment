var webApp = angular.module('webApp', ['ui.router', 'ngSanitize', 'rest', 'data-service', 'controllers', 'list']);


webApp.config(function($stateProvider, $urlRouterProvider) {
    // default load all categories page
    $urlRouterProvider.otherwise('/category/departments');
    // routing  for categories and product view
    $stateProvider
            .state('category', {
                url: '/category/:categoryId',
                views: {
                    'categories-view': {
                        templateUrl: '/html/categories/list.html',
                        controller: 'categoriesController'
                    },
                    'category-products-view': {
                        templateUrl: '/html/products/list.html',
                        controller: 'productsController'
                    }
                }
            }).state('category.product', {
        url: '/:sku',
        views: {
            'product-view': {
                templateUrl: '/html/products/view.html',
                controller: 'productController'
            }
        }
    });
});
// Urls Configuration
webApp.constant('baseUrls', {'siteUrl': 'http://www.bestbuy.ca', 'apiUrl': 'http://www.bestbuy.ca/api/v2/json'});

